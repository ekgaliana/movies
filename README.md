# NgMovies

Responsive SPA

Stack : Typescript, Angular, Rxjs, NgRx

## Quick Start

Open one terminal window and run :
run json-server --watch db.json --delay 1000

Additional to that window terminal open another one and run :
npm start
