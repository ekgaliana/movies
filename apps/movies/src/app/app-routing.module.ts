// Angular
import { NgModule } from '@angular/core';

// Angular Router
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'movies',
    pathMatch: 'full',
  },
  {
    path: 'movies',
    loadChildren: () =>
      import('./public/movies/movies.module').then((m) => m.MoviesModule),
    data: { preload: true },
  },
  {
    path: 'actors',
    loadChildren: () =>
      import('./public/actors/actors.module').then((m) => m.ActorsModule),
    data: { preload: true },
  },
  {
    path: 'studios',
    loadChildren: () =>
      import('./public/studios/studios.module').then((m) => m.StudiosModule),
    data: { preload: true },
  },
  {
    path: '**',
    redirectTo: 'movies',
    pathMatch: 'full',
  },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules }),
  ],
  exports: [RouterModule],
})
export class AppRoutingModule {}
