// Angular
import { NgModule } from '@angular/core';

// Router
import { Routes, RouterModule } from '@angular/router';

// View
import { StudiosViewComponent } from './views/studios.view';

const routes: Routes = [
  {
    path: '',
    component: StudiosViewComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class StudiosViewRoutingModule {}
