// Angular
import {
  ChangeDetectionStrategy,
  Component,
  ViewEncapsulation,
} from '@angular/core';

// Store
import { MoviesFacade } from '@movies/services';

@Component({
  selector: 'movies-studios-view',
  templateUrl: './studios.view.html',
  styleUrls: ['./studios.view.scss'],
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class StudiosViewComponent {
  constructor(public moviesFacade: MoviesFacade) {}
}
