import { TestBed } from '@angular/core/testing';
import { StudiosViewComponent } from './studios.view';

describe('StudiosViewComponent', () => {
  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [StudiosViewComponent],
    }).compileComponents();
  });

  it('should create studios component', () => {
    const fixture = TestBed.createComponent(StudiosViewComponent);
    const app = fixture.componentInstance;
    expect(app).toBeTruthy();
  });
});
