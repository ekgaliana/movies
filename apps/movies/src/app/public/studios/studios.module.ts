// Angular
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

// Store
import { MoviesFacade } from '@movies/services';

// Routing
import { StudiosViewRoutingModule } from './studios-routing.module';

// View
import { StudiosViewComponent } from './views/studios.view';

@NgModule({
  declarations: [StudiosViewComponent],
  imports: [CommonModule, StudiosViewRoutingModule],
  providers: [MoviesFacade],
})
export class StudiosModule {}
