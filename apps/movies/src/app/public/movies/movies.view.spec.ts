import { TestBed } from '@angular/core/testing';
import { MoviesViewComponent } from './movies.view';

describe('MoviesViewComponent', () => {
  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [MoviesViewComponent],
    }).compileComponents();
  });

  it('should create movies component', () => {
    const fixture = TestBed.createComponent(MoviesViewComponent);
    const app = fixture.componentInstance;
    expect(app).toBeTruthy();
  });
});
