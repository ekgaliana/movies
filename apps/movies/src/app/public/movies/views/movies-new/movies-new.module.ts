// Angular
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

// Forms
import { ReactiveFormsModule } from '@angular/forms';

// Store
import { MoviesFacade } from '@movies/services';

// Router
import { MoviesNewViewRoutingModule } from './movies-new-routing.module';

// Views
import { MoviesNewViewComponent } from './movies-new.view';

// Ui Cimponent
import { MaterialModule } from '../../../../material/angular-material.module';
import { ProgressSpinnerModule } from 'primeng/progressspinner';

@NgModule({
  declarations: [MoviesNewViewComponent],
  imports: [
    CommonModule,
    MoviesNewViewRoutingModule,
    ReactiveFormsModule,
    ProgressSpinnerModule,
    MaterialModule,
  ],
  providers: [MoviesFacade],
})
export class MoviesNewViewModule {}
