// Angular
import {
  ChangeDetectionStrategy,
  Component,
  OnDestroy,
  OnInit,
  ViewEncapsulation,
} from '@angular/core';

// Angular Forms
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

// Rxjs
import { debounceTime, filter, Subject, takeUntil } from 'rxjs';

// Facade
import { MoviesFacade } from '@movies/services';

@Component({
  selector: 'movies-new-view',
  templateUrl: './movies-new.view.html',
  styleUrls: ['./movies-new.view.scss'],
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class MoviesNewViewComponent implements OnInit, OnDestroy {
  moviesForm: FormGroup = new FormGroup({});

  actorsList: number[] = [1, 2, 3];

  genresList: string[] = ['comedy', 'drama', 'familiar', 'adventures'];

  selectedGenres: string[] = [];
  selectedActors: number[] = [];

  private destroyed$: Subject<boolean> = new Subject<boolean>();

  constructor(
    public moviesFacade: MoviesFacade,
    private formBuilder: FormBuilder
  ) {}

  ngOnInit(): void {
    this.initForm();
    this.buildFormSubscriptions();
    this.buildDataToFormSubscriptions();
  }

  ngOnDestroy(): void {
    this.clearDestroyedSubscription();
  }

  doPostNewMovie() {
    if (!this.moviesForm.valid) {
      return;
    }
    this.moviesFacade.postNewMovie();
  }

  resetPostSuccess(): void {
    this.moviesFacade.resetPostSuccess();
  }

  resetError(): void {
    this.moviesFacade.resetError();
  }

  private initForm() {
    this.moviesForm = this.formBuilder.group({
      title: [null, Validators.required],
      poster: [null, Validators.required],
      genre: [null, Validators.required],
      year: [null, Validators.required],
      duration: [null, Validators.required],
      imdbRating: [null, Validators.required],
      actors: [null, Validators.required],
    });
  }

  private buildFormSubscriptions() {
    this.buildTitleFormSubscriptions();
    this.buildPosterFormSubscriptions();
    this.buildGenreFormSubscriptions();
    this.buildYearFormSubscriptions();
    this.buildDurationFormSubscriptions();
    this.buildImdbRatingFormSubscriptions();
    this.buildActorsFormSubscriptions();
  }

  private buildTitleFormSubscriptions(): void {
    this.moviesForm
      .get('title')
      ?.valueChanges.pipe(
        debounceTime(300),
        filter((title) => !!title),
        takeUntil(this.destroyed$)
      )
      .subscribe((title) => this.moviesFacade.updateTitle(title));
  }

  private buildPosterFormSubscriptions(): void {
    this.moviesForm
      .get('poster')
      ?.valueChanges.pipe(
        debounceTime(300),
        filter((poster) => !!poster),
        takeUntil(this.destroyed$)
      )
      .subscribe((poster) => this.moviesFacade.updatePoster(poster));
  }

  private buildGenreFormSubscriptions(): void {
    this.moviesForm
      .get('genre')
      ?.valueChanges.pipe(
        debounceTime(300),
        filter((genre) => !!genre),
        takeUntil(this.destroyed$)
      )
      .subscribe((genre: string) => {
        this.selectedGenres = [...this.selectedGenres, genre];
        this.moviesFacade.updateGenre(this.selectedGenres);
      });
  }

  private buildYearFormSubscriptions(): void {
    this.moviesForm
      .get('year')
      ?.valueChanges.pipe(
        debounceTime(300),
        filter((year) => !!year),
        takeUntil(this.destroyed$)
      )
      .subscribe((year) => this.moviesFacade.updateYear(year));
  }

  private buildDurationFormSubscriptions(): void {
    this.moviesForm
      .get('duration')
      ?.valueChanges.pipe(
        debounceTime(300),
        filter((duration) => !!duration),
        takeUntil(this.destroyed$)
      )
      .subscribe((duration) => this.moviesFacade.updateDuration(duration));
  }

  private buildImdbRatingFormSubscriptions(): void {
    this.moviesForm
      .get('imdbRating')
      ?.valueChanges.pipe(
        debounceTime(300),
        filter((imdbRating) => !!imdbRating),
        takeUntil(this.destroyed$)
      )
      .subscribe((imdbRating) =>
        this.moviesFacade.updateimdbRating(imdbRating)
      );
  }

  private buildActorsFormSubscriptions(): void {
    this.moviesForm
      .get('actors')
      ?.valueChanges.pipe(
        filter((actor) => !!actor),
        takeUntil(this.destroyed$)
      )
      .subscribe((actor: string) => {
        this.selectedActors = [...this.selectedActors, +actor];
        this.moviesFacade.updateActors(this.selectedActors);
      });
  }

  /**
   * Data from subscriptions sets data from
   * storage to the form to kee the store
   * as unique source of data.
   * Due time constrains some form fields
   * are pending to implement to this subscriptions
   *  */
  private buildDataToFormSubscriptions(): void {
    this.buildTitleDataSubscription();
    this.buildYearDataSubscription();
  }

  private buildTitleDataSubscription(): void {
    this.moviesFacade.title$
      .pipe(takeUntil(this.destroyed$))
      .subscribe((title) => {
        this.moviesForm.patchValue({ title });
      });
  }

  private buildYearDataSubscription(): void {
    this.moviesFacade.year$
      .pipe(takeUntil(this.destroyed$))
      .subscribe((year) => {
        this.moviesForm.patchValue({ year });
      });
  }

  private clearDestroyedSubscription() {
    this.destroyed$.next(true);
    this.destroyed$.complete();
  }
}
