import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { MoviesNewViewComponent } from './movies-new.view';

const routes: Routes = [
  {
    path: '',
    component: MoviesNewViewComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class MoviesNewViewRoutingModule {}
