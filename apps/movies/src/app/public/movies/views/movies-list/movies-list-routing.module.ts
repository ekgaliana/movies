// Angular
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

// Views
import { MoviesListViewComponent } from './movies-list.view';

const routes: Routes = [
  {
    path: '',
    component: MoviesListViewComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class MoviesListViewRoutingModule {}
