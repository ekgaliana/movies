// Angular
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

// Store
import { MoviesFacade, MoviesStoreModule } from '@movies/services';

// Views
import { MoviesListViewComponent } from './movies-list.view';

// Router
import { MoviesListViewRoutingModule } from './movies-list-routing.module';

// UI Components
import { ProgressSpinnerModule } from 'primeng/progressspinner';
import { MaterialModule } from '../../../../material/angular-material.module';

@NgModule({
  declarations: [MoviesListViewComponent],
  imports: [
    CommonModule,
    MoviesListViewRoutingModule,
    MoviesStoreModule,
    ProgressSpinnerModule,
    MaterialModule,
  ],
  providers: [MoviesFacade],
})
export class MoviesListViewModule {}
