// Angular
import {
  ChangeDetectionStrategy,
  Component,
  OnInit,
  ViewEncapsulation,
} from '@angular/core';

// Store
import { MoviesFacade } from '@movies/services';
import { MoviesModel } from '@movies/models';

@Component({
  selector: 'movies-list-view',
  templateUrl: './movies-list.view.html',
  styleUrls: ['./movies-list.view.scss'],
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class MoviesListViewComponent implements OnInit {
  constructor(public moviesFacade: MoviesFacade) {}

  ngOnInit(): void {
    this.moviesFacade.loadMoviesData();
  }

  movieListTrackBy(index: number, item: MoviesModel): number {
    return item.id;
  }

  selectedMovieId(id: number): void {
    this.moviesFacade.setSelectedMovie(id);
  }

  resetError(): void {
    this.moviesFacade.resetError();
  }
}
