// Angular
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

// Store
import { MoviesFacade } from '@movies/services';

// Routing
import { MoviesDetailsViewRoutingModule } from './movies-details-routing.module';

// Views
import { MoviesDetailsViewComponent } from './movies-details.view';

// Ui Components
import { MaterialModule } from '../../../../material/angular-material.module';

@NgModule({
  declarations: [MoviesDetailsViewComponent],
  imports: [CommonModule, MoviesDetailsViewRoutingModule, MaterialModule],
  providers: [MoviesFacade],
})
export class MoviesDetailsViewModule {}
