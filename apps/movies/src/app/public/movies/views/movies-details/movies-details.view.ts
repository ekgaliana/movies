// Angular
import {
  ChangeDetectionStrategy,
  Component,
  ViewEncapsulation,
} from '@angular/core';

// Store
import { MoviesFacade } from '@movies/services';

@Component({
  selector: 'movies-details-view',
  templateUrl: './movies-details.view.html',
  styleUrls: ['./movies-details.view.scss'],
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class MoviesDetailsViewComponent {
  constructor(public moviesFacade: MoviesFacade) {}
}
