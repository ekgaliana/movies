// Angular
import { NgModule } from '@angular/core';

// Router
import { Routes, RouterModule } from '@angular/router';

// Views
import { MoviesDetailsViewComponent } from './movies-details.view';

const routes: Routes = [
  {
    path: '',
    component: MoviesDetailsViewComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class MoviesDetailsViewRoutingModule {}
