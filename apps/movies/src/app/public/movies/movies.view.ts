// Angular
import {
  ChangeDetectionStrategy,
  Component,
  ViewEncapsulation,
} from '@angular/core';

// Store
import { MoviesFacade } from '@movies/services';

@Component({
  selector: 'movies-view',
  templateUrl: './movies.view.html',
  styleUrls: ['./movies.view.scss'],
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class MoviesViewComponent {
  constructor(public moviesFacade: MoviesFacade) {}
}
