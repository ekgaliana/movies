// Angular
import { NgModule } from '@angular/core';

// Router
import { Routes, RouterModule } from '@angular/router';

// Views
import { MoviesViewComponent } from './movies.view';

const routes: Routes = [
  {
    path: '',
    component: MoviesViewComponent,
    children: [
      {
        path: '',
        redirectTo: 'list',
        pathMatch: 'full',
      },
      {
        path: 'list',
        loadChildren: () =>
          import('./views/movies-list/movies-list.module').then(
            (m) => m.MoviesListViewModule
          ),
        data: { preload: true },
      },
      {
        path: 'details',
        loadChildren: () =>
          import('./views/movies-details/movies-detais.module').then(
            (m) => m.MoviesDetailsViewModule
          ),
        data: { preload: true },
      },
      {
        path: 'new',
        loadChildren: () =>
          import('./views/movies-new/movies-new.module').then(
            (m) => m.MoviesNewViewModule
          ),
        data: { preload: true },
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class MoviesViewRoutingModule {}
