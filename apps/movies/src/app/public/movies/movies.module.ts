// Angular
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

// Store
import { MoviesFacade } from '@movies/services';

// Routing
import { MoviesViewRoutingModule } from './movies-routing.module';

// Views
import { MoviesViewComponent } from './movies.view';

@NgModule({
  declarations: [MoviesViewComponent],
  imports: [CommonModule, MoviesViewRoutingModule],
  providers: [MoviesFacade],
})
export class MoviesModule {}
