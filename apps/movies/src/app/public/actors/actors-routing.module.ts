// Angular
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

// Views
import { ActorsViewComponent } from './views/actors.view';

const routes: Routes = [
  {
    path: '',
    component: ActorsViewComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ActorsViewRoutingModule {}
