import { TestBed } from '@angular/core/testing';
import { ActorsViewComponent } from './actors.view';

describe('ActorsViewComponent', () => {
  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ActorsViewComponent],
    }).compileComponents();
  });

  it('should create the Actors component', () => {
    const fixture = TestBed.createComponent(ActorsViewComponent);
    const app = fixture.componentInstance;
    expect(app).toBeTruthy();
  });
});
