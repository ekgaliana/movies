// Angular
import {
  ChangeDetectionStrategy,
  Component,
  ViewEncapsulation,
} from '@angular/core';

// Store
import { MoviesFacade } from '@movies/services';

@Component({
  selector: 'movies-actors-view',
  templateUrl: './actors.view.html',
  styleUrls: ['./actors.view.scss'],
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ActorsViewComponent {
  constructor(public moviesFacade: MoviesFacade) {}
}
