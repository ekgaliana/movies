// Angular
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

// Store
import { MoviesFacade } from '@movies/services';

// Routing
import { ActorsViewRoutingModule } from './actors-routing.module';

// View
import { ActorsViewComponent } from './views/actors.view';

@NgModule({
  declarations: [ActorsViewComponent],
  imports: [CommonModule, ActorsViewRoutingModule],
  providers: [MoviesFacade],
})
export class ActorsModule {}
