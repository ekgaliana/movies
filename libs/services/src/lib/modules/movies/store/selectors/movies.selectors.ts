// NgRx
import { createFeatureSelector, createSelector } from '@ngrx/store';

// Store
import { MoviesState } from '../reducers/reducer-map';

export const getMoviesState = createFeatureSelector<MoviesState>('movies');

// VIEW DATA SELECTORS
export const getMoviesDataState = createSelector(
  getMoviesState,
  (state) => state.moviesDataState
);

export const getMovies = createSelector(
  getMoviesDataState,
  (state) => state.movies
);

export const getisMoviesLoaded = createSelector(
  getMoviesDataState,
  (state) => !!state.movies
);

export const getSelectedMovie = createSelector(
  getMoviesDataState,
  (state) => state.selectedMovie
);

export const getErrorNotification = createSelector(
  getMoviesDataState,
  (state) => state.errors
);

export const getPostSuccess = createSelector(
  getMoviesDataState,
  (state) => state.postSuccess
);

export const getisLoading = createSelector(
  getMoviesDataState,
  (state) => state.loading
);

// VIEW FORM SELECTORS

export const getMoviesFormDataState = createSelector(
  getMoviesState,
  (state) => state.moviesFormDataState
);

export const getTitle = createSelector(
  getMoviesFormDataState,
  (state) => state.title
);

export const getPoster = createSelector(
  getMoviesFormDataState,
  (state) => state.poster
);

export const getGenre = createSelector(
  getMoviesFormDataState,
  (state) => state.genre
);

export const getYear = createSelector(
  getMoviesFormDataState,
  (state) => state.year
);

export const getDuration = createSelector(
  getMoviesFormDataState,
  (state) => state.duration
);

export const getimdbRating = createSelector(
  getMoviesFormDataState,
  (state) => state.imdbRating
);

export const getActors = createSelector(
  getMoviesFormDataState,
  (state) => state.actors
);
