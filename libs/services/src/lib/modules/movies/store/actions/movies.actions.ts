// NgRx
import { createAction, props } from '@ngrx/store';

// Model
import { MoviesModel } from '../../../../models/movies.models';

export const loadMoviesData = createAction('[Movies] Load Movies Data');

export const loadMoviesDataSuccess = createAction(
  '[Movies] Load Movies Data Success',
  props<{ payload: MoviesModel[] }>()
);

export const loadMoviesDataFail = createAction(
  '[Movies] Load Movies Data Fail',
  props<{ error: any }>()
);

export const setSelectedMovie = createAction(
  '[Movies] Set Selected Movie',
  props<{ payload: MoviesModel }>()
);

export const showLoader = createAction('[Movies] Show Loader');

export const hideLoader = createAction('[Movies] Hide Loader');

// UPDATE FORMS ACTIONS
export const updateForm = createAction(
  '[Movies] Update Form',
  props<{
    [key: string]: any;
  }>()
);

export const resetPostSuccess = createAction('[Movies] Reset Post Success');

export const resetError = createAction('[Movies] Reset Error');

// POST ACTION

export const doPostNewMovies = createAction(
  '[Movies] Post New',
  props<{ request: MoviesModel }>()
);

export const doPostNewMoviesSuccess = createAction(
  '[Movies] Post New Movie Success'
);

export const doPostNewMoviesFailure = createAction(
  '[Movies] Post New Movie Failure',
  props<{ error: any }>()
);

// CLEAR ACTIONS

export const clearMoviesData = createAction('[Movies] Clear Movies Data Sore');

export const clearMoviesFormData = createAction(
  '[Movies] Clear Movies Form Data Sore'
);
