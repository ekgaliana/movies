// NgRx
import { ActionReducerMap } from '@ngrx/store';

// Store
import {
  moviesDataReducerFunction,
  MoviesDataState,
  moviesFormDataReducerFunction,
  MoviesFormDataState,
} from './movies.reducer';

export interface MoviesState {
  moviesDataState: MoviesDataState;
  moviesFormDataState: MoviesFormDataState;
}

export const moviesReducers: ActionReducerMap<MoviesState> = {
  moviesDataState: moviesDataReducerFunction,
  moviesFormDataState: moviesFormDataReducerFunction,
};
