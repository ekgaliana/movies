// NgRx
import { Action, createReducer, on } from '@ngrx/store';

// Store
import { MoviesActions } from '../actions/action-types';

// Models
import { MoviesModel } from '../../../../models/movies.models';

// DATA VIEW STATE
export interface MoviesDataState {
  movies: MoviesModel[];
  selectedMovie: MoviesModel;
  errors: any;
  postSuccess: boolean;
  loading: boolean;
}

export const initialMovieDataState: MoviesDataState = {
  movies: null,
  selectedMovie: null,
  errors: null,
  postSuccess: false,
  loading: false,
};

const moviesDataReducer = createReducer(
  initialMovieDataState,
  on(MoviesActions.loadMoviesDataSuccess, (state, action) => {
    return {
      ...state,
      movies: action.payload,
      errors: null,
    };
  }),
  on(MoviesActions.loadMoviesDataFail, (state, action) => {
    return {
      ...state,
      errors: action.error,
    };
  }),
  on(MoviesActions.setSelectedMovie, (state, action) => {
    return {
      ...state,
      selectedMovie: action.payload,
    };
  }),
  on(MoviesActions.doPostNewMoviesSuccess, (state) => {
    return {
      ...state,
      postSuccess: true,
      errors: null,
    };
  }),
  on(MoviesActions.doPostNewMoviesFailure, (state, action) => {
    return {
      ...state,
      postSuccess: false,
      errors: action.error,
    };
  }),
  on(MoviesActions.resetPostSuccess, (state) => {
    return {
      ...state,
      postSuccess: null,
    };
  }),
  on(MoviesActions.resetError, (state) => {
    return {
      ...state,
      errors: null,
    };
  }),
  on(MoviesActions.showLoader, (state) => {
    return {
      ...state,
      loading: true,
    };
  }),
  on(MoviesActions.hideLoader, (state) => {
    return {
      ...state,
      loading: false,
    };
  }),
  on(MoviesActions.clearMoviesData, () => {
    return initialMovieDataState;
  })
);

export function moviesDataReducerFunction(
  state: MoviesDataState | undefined,
  action: Action
) {
  return moviesDataReducer(state, action);
}

// FORM VIEW STATE

export interface MoviesFormDataState {
  title: string;
  poster: string;
  genre: string[];
  year: number;
  duration: number;
  imdbRating: number;
  actors: number[];
}

export const initialFormState: MoviesFormDataState = {
  title: null,
  poster: null,
  genre: null,
  year: null,
  duration: null,
  imdbRating: null,
  actors: null,
};

const moviesFormDataReducer = createReducer(
  initialFormState,
  on(MoviesActions.updateForm, (state, action) => {
    const actionValue = {
      [Object.keys(action)[0]]: action[Object.keys(action)[0]],
    };
    return Object.assign({}, state, actionValue);
  }),
  on(MoviesActions.clearMoviesFormData, () => {
    return initialFormState;
  })
);

export function moviesFormDataReducerFunction(
  state: MoviesFormDataState | undefined,
  action: Action
) {
  return moviesFormDataReducer(state, action);
}
