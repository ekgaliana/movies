// Angular
import { Injectable } from '@angular/core';

// Rxjs
import { exhaustMap, map, of } from 'rxjs';
import { catchError } from 'rxjs/operators';

// NgRx
import { createEffect, Actions, ofType } from '@ngrx/effects';

// Api
import { MoviesApi } from '../../../../apis/moviesApi';

// Store
import { MoviesActions } from '../actions/action-types';

// Loader Actions Const
const SHOW_LOADER_ACTIONS = [
  MoviesActions.loadMoviesData,
  MoviesActions.doPostNewMovies,
];
const HIDE_LOADER_ACTIONS = [
  MoviesActions.loadMoviesDataSuccess,
  MoviesActions.loadMoviesDataFail,
  MoviesActions.doPostNewMoviesSuccess,
  MoviesActions.doPostNewMoviesFailure,
];
@Injectable()
export class MoviesEffects {
  constructor(
    private readonly actions$: Actions,
    private moviesApi: MoviesApi
  ) {}

  getMovieData$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(MoviesActions.loadMoviesData),
      exhaustMap(() => {
        return this.moviesApi.getMovies().pipe(
          map((response) => {
            return MoviesActions.loadMoviesDataSuccess({ payload: response });
          }),
          catchError((error) => {
            return of(MoviesActions.loadMoviesDataFail({ error }));
          })
        );
      })
    );
  });

  postNewMovie$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(MoviesActions.doPostNewMovies),
      exhaustMap((action) => {
        return this.moviesApi.postMovie(action.request).pipe(
          map(() => {
            return MoviesActions.doPostNewMoviesSuccess();
          }),
          catchError((error) => {
            return of(MoviesActions.doPostNewMoviesFailure({ error }));
          })
        );
      })
    );
  });

  showLoader$ = createEffect(() =>
    this.actions$.pipe(
      ofType(...SHOW_LOADER_ACTIONS),
      map(() => MoviesActions.showLoader())
    )
  );

  hideLoader$ = createEffect(() =>
    this.actions$.pipe(
      ofType(...HIDE_LOADER_ACTIONS),
      map(() => MoviesActions.hideLoader())
    )
  );
}
