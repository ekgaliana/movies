// Angular
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';

// Ngrx
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';

// Store
import { MoviesFacade } from './facades/movies.facade';
import { moviesReducers } from './store/reducers/reducer-map';
import { MoviesEffects } from './store/effects/movies.effects';

@NgModule({
  imports: [
    CommonModule,
    HttpClientModule,
    StoreModule.forFeature('movies', moviesReducers),
    EffectsModule.forFeature([MoviesEffects]),
  ],
  providers: [MoviesFacade],
})
export class MoviesStoreModule {}
