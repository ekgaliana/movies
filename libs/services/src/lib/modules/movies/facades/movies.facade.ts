// Angular
import { Injectable } from '@angular/core';

// RxJs
import { Observable, take, withLatestFrom } from 'rxjs';

// NgRx
import { select, Store } from '@ngrx/store';
import { Actions, ofType } from '@ngrx/effects';

// Store
import { MoviesState } from '../store/reducers/reducer-map';
import * as fromSelectors from '../store/selectors/movies.selectors';
import { MoviesActions } from '../store/actions/action-types';
import { MoviesFormDataState } from '../store/reducers/movies.reducer';

// Models
import { MoviesModel } from '../../../models/movies.models';

@Injectable()
export class MoviesFacade {
  // DATA OBSERVABLES
  movies$: Observable<MoviesModel[]> = this.parentStore.pipe(
    select(fromSelectors.getMovies)
  );

  isMoviesLoaded$: Observable<boolean> = this.parentStore.pipe(
    select(fromSelectors.getisMoviesLoaded)
  );

  selectedMovie$: Observable<MoviesModel> = this.parentStore.pipe(
    select(fromSelectors.getSelectedMovie)
  );

  error$: Observable<any> = this.parentStore.pipe(
    select(fromSelectors.getErrorNotification)
  );

  postSuccess$: Observable<boolean> = this.parentStore.pipe(
    select(fromSelectors.getPostSuccess)
  );

  showLoader$: Observable<boolean> = this.parentStore.pipe(
    select(fromSelectors.getisLoading)
  );

  // FORM OBSERVABLES

  formData$: Observable<MoviesFormDataState> = this.parentStore.pipe(
    select(fromSelectors.getMoviesFormDataState)
  );

  title$: Observable<string | undefined> = this.parentStore.pipe(
    select(fromSelectors.getTitle)
  );

  poster$: Observable<string | undefined> = this.parentStore.pipe(
    select(fromSelectors.getPoster)
  );

  genre$: Observable<string[] | undefined> = this.parentStore.pipe(
    select(fromSelectors.getGenre)
  );

  year$: Observable<number | undefined> = this.parentStore.pipe(
    select(fromSelectors.getYear)
  );

  duration$: Observable<number> = this.parentStore.pipe(
    select(fromSelectors.getDuration)
  );

  imdbRating$: Observable<number> = this.parentStore.pipe(
    select(fromSelectors.getimdbRating)
  );

  actors$: Observable<number[]> = this.parentStore.pipe(
    select(fromSelectors.getActors)
  );

  constructor(
    private parentStore: Store<MoviesState>,
    private parentUpdates$: Actions
  ) {
    this.reloadMoviesSubscription();
  }

  loadMoviesData(): void {
    this.isMoviesLoaded$.pipe(take(1)).subscribe((data) => {
      if (data) {
        return;
      }
      this.parentStore.dispatch(MoviesActions.loadMoviesData());
    });
  }

  postNewMovie(): void {
    this.formData$
      .pipe(take(1), withLatestFrom(this.movies$))
      .subscribe(([formdata, movies]: [MoviesFormDataState, MoviesModel[]]) => {
        const request: any = {
          id: movies.length + 1,
          title: formdata.title,
          poster: formdata.poster,
          genre: formdata.genre,
          year: formdata.year,
          duration: formdata.duration,
          imdbRating: formdata.imdbRating,
          actors: formdata.actors,
        };
        this.parentStore.dispatch(MoviesActions.doPostNewMovies({ request }));
      });
  }

  reloadMoviesSubscription() {
    this.parentUpdates$
      .pipe(ofType(MoviesActions.doPostNewMoviesSuccess), take(1))
      .subscribe(() =>
        this.parentStore.dispatch(MoviesActions.loadMoviesData())
      );
  }

  setSelectedMovie(id: number) {
    this.movies$.pipe(take(1)).subscribe((movies) => {
      const selectedMovie: MoviesModel = movies.find(
        (movie) => movie.id === id
      );
      this.parentStore.dispatch(
        MoviesActions.setSelectedMovie({ payload: selectedMovie })
      );
    });
  }

  updateTitle(title: string): void {
    this.parentStore.dispatch(MoviesActions.updateForm({ title }));
  }

  updatePoster(poster: string): void {
    this.parentStore.dispatch(MoviesActions.updateForm({ poster }));
  }

  updateGenre(genre: string[]): void {
    this.parentStore.dispatch(MoviesActions.updateForm({ genre }));
  }

  updateYear(year: number): void {
    this.parentStore.dispatch(MoviesActions.updateForm({ year }));
  }

  updateDuration(duration: number): void {
    this.parentStore.dispatch(MoviesActions.updateForm({ duration }));
  }

  updateimdbRating(imdbRating: number): void {
    this.parentStore.dispatch(MoviesActions.updateForm({ imdbRating }));
  }

  updateActors(actors: number[]): void {
    this.parentStore.dispatch(MoviesActions.updateForm({ actors }));
  }

  resetPostSuccess(): void {
    this.parentStore.dispatch(MoviesActions.resetPostSuccess());
  }

  resetError(): void {
    this.parentStore.dispatch(MoviesActions.resetError());
  }
}
