// Angular
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

// RxJs
import { Observable } from 'rxjs';

// Interfaces
import { MoviesModel } from '../models/movies.models';

@Injectable({
  providedIn: 'root',
})
export class MoviesApi {
  private baseUrl = 'http://localhost:3000/'; // URL to web api

  constructor(private httpClient: HttpClient) {}

  /** GET Movies from the server */
  getMovies(): Observable<MoviesModel[]> {
    return this.httpClient.get<MoviesModel[]>(this.baseUrl + 'movies');
  }

  postMovie(movie: MoviesModel): Observable<MoviesModel> {
    return this.httpClient.post<MoviesModel>(this.baseUrl + 'movies', movie);
  }
}
