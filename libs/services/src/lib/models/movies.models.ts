import { Model } from './model';

export declare class MoviesModel extends Model {
  id: number;
  title: string;
  poster: string;
  genre: string[];
  year: number;
  duration: number;
  imdbRating: number;
  actors: number[];
  constructor(attrs?: MoviesModelAttrs);
}

export default interface MoviesModelAttrs {
  id?: number;
  title?: string;
  poster?: string;
  genre?: string[];
  year?: number;
  duration?: number;
  imdbRating?: number;
  actors?: number[];
}
