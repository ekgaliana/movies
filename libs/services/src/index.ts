// Movies
export { MoviesStoreModule } from './lib/modules/movies/movies.module';
export { MoviesFacade } from './lib/modules/movies/facades/movies.facade';
